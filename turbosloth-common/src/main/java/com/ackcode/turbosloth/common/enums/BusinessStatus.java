package com.ackcode.turbosloth.common.enums;

/**
 * 操作状态
 * 
 * @author mxpio
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
