package com.ackcode.turbosloth.common.exception;

/**
 * 演示模式异常
 * 
 * @author mxpio
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
