# Turbosloth-Boot

## 平台简介

> Turbosloth的名字源自于Turbo(涡轮增压)和Sloth(树懒)，我们的愿景是打造一套高效、简单、强大的开发平台。让开发者像装配了涡轮增压一样高效，工作像树懒一样惬意。

* 前端采用Vue、AntD。
* 后端采用Spring Boot、Spring Security、Redis & Jwt。
* 权限认证使用Jwt，支持多终端认证系统。
* 支持加载动态权限菜单，多方式轻松权限控制。
* 高效率开发，使用代码生成器可以一键生成前后端代码。

## 资源

* 使用文档：https://gitee.com/ackcode/turbosloth/wikis
* 前端地址：https://gitee.com/ackcode/turbosloth-antd-vue
* 后端地址：https://gitee.com/ackcode/turbosloth
* 演示地址：准备中...

## 内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线用户：当前系统中活跃用户状态监控。
12. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
13. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
14. 系统接口：根据业务代码自动生成相关的api接口文档。
15. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
16. 在线构建器：拖动表单元素生成相应的HTML代码。
17. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

## 技术栈

工具 | 版本
---|---
Spring Boot | 2.1.1.RELEASE
Spring framework | 5.1.3.RELEASE
Mybatis | 3.4.6
Quartz | 2.3.0
Swagger | 2.9.2
Druid | 1.1.23
Jackson | 2.9.7
Poi | 3.17
Velocity | 1.7
Slf4j | 1.7.25
JWT | 0.9.0

## 使用
### clone
```bash
$ git https://gitee.com/ackcode/turbosloth.git
```
### 启动
```bash
$ cd $REDIS_PATH
$ ./redis-server redis.conf 
$ cd turbosloth
$ maven clean package
$ cd turbosloth-admin/target/
$ java -jar turbosloth-admin-1.0.0.jar
```
