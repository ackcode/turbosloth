package com.ackcode.turbosloth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 * 
 * @author mxpio
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class TurboslothApplication
{
    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(TurboslothApplication.class, args);
    }
}
